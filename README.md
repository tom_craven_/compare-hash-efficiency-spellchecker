Spellcheck - hash method comparision

enter some words in test.txt to spell check them against the words in dictionary.txt

The system compare various hash and collision avoidance methods by various table sizes and provide statistical results including a crude distribution map.

Todo: 

remove case sensitivity

realtime time efficiency is a poor measure as it will vary with the system and processor employed.
Better to make a static analysis of the complexity multiplied by the hop count average. 
The current system greedily records the highest hop count(poorest performance) discarding the rest, this requires redevelopment.
